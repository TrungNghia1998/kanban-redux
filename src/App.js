import React, { Component } from "react";
import "./App.css";
import { Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import UserForm from "./components/UserForm";
import ListTask from "./components/ListTask/ListTask";
import FormTask from "./components/FormTask/FormTask";

// import * as actions from "./store/actions/index";

class App extends Component {

  render() {
    let routes = (
      <Switch>
        <Route path="/" exact component={UserForm} />
        <Route path="/tasks" component={ListTask} />
        <Route path="/task/:id" component={FormTask}/>
        <Redirect to="/" />
      </Switch>
    );

    return (
      <div className="App">
        {routes}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    tasks: state.task.tasks,
  };
};

export default connect(mapStateToProps)(App);
