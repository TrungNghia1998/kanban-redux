import * as actionTypes from "./actionTypes";

export const onChange = event => {
    return {
        type: actionTypes.ON_CHANGE,
        event: event
    };
};

export const onChangeTaskForm = event => {
    return {
        type: actionTypes.ON_CHANGE_TASK_FORM,
        event: event
    };
};

export const onChangeDetail = (event, data) => {
    return {
        type: actionTypes.ON_CHANGE_TASK_FORM,
        event: event,
        data: data
    };
};

export const onChangeInformationUser = event => {
    return {
        type: actionTypes.ON_CHANGE_INFORMATION_USER,
        event: event
    };
};

export const onClearForm = () => {
    return {
        type: actionTypes.ON_CLEAR_FORM
    };
};

export const onToggleModal = () => {
    return {
        type: actionTypes.ON_TOGGLE_MODAL
    };
};

export const onRegisterHandler = (user) => {
    return {
        type: actionTypes.ON_REGISTER_HANDLER,
        user: user
    };
};

export const onAddTask = (task) => {
    return {
        type: actionTypes.ON_ADD_TASK,
        task: task
    };
};

export const onCloseModal = () => {
    return {
        type: actionTypes.ON_CLOSE_MODAL
    };
};

export const onToggleModalSwap = (id) => {
    return {
        type: actionTypes.ON_TOGGLE_MODAL_SWAP,
        id: id
    };
};


export const onToggleModalEditUser = (event) => {
    return {
        type: actionTypes.ON_TOGGLE_MODAL_EDIT_USER,
        event: event
    };
};

export const onUpdatePosition = (data) => {
    return {
        type: actionTypes.ON_UPDATE_POSITION,
        data: data
    };
};

export const onDeleteTask = (id) => {
    return {
        type: actionTypes.ON_DELETE_TASK,
        id: id
    };
};

export const onUpdateUser = (data) => {
    return {
        type: actionTypes.ON_UPDATE_USER,
        data: data
    };
};

export const onCreateTask = (event, task) => {
    return {
        type: actionTypes.ON_ADD_TASK,
        task: task,
        event: event
    };
};



