import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../../shared/utility";

const initialState = {
  toggleModal: false,
  toggleModalSwap: false,
  showModalSwap: false,
  tasks: [],
  name: "",
  description: "",
  idTaskSwap: null,
  position: 1,
  closeModal: false,
};

const onToggleModal = state => {
  return updateObject(state, {
    toggleModal: !state.toggleModal,
  });
};

const onAddTask = (state, action) => {
  let tasks = JSON.parse(localStorage.getItem("List Tasks"));
  if (!tasks) tasks = [];
  // let tasks = JSON.parse(localStorage.getItem("List Tasks"));
  const task = action.task;
  let newTask = {
    name: task.name,
    description: task.description,
    position: 1,
    id: Math.random() + Math.random(),
  };
  tasks.push(newTask);
  localStorage.setItem("List Tasks", JSON.stringify(tasks));
  return updateObject(state, {
    tasks: tasks,
    toggleModal: !state.toggleModal,
  });
};

const onCloseModal = state => {
  return updateObject(state, {
    name: "",
    description: "",
    closeModal: true,
  });
};

const onToggleModalSwap = (state, action) => {
  console.log(123);
  return updateObject(state, {
    showModalSwap: !state.showModalSwap,
    toggleModalSwap: !state.toggleModalSwap,
    idTaskSwap: action.id,
  });
};

const findIndex = (state, id) => {
  let { tasks } = state;
  let result = -1;
  tasks.forEach((task, index) => {
    if (task.id === id) {
      result = index;
    }
  });
  return result;
};

const onUpdatePosition = (state, action) => {
  let { idTaskSwap } = state;
  console.log(idTaskSwap);
  // let tasks = [...state.tasks];
  let tasks = JSON.parse(localStorage.getItem("List Tasks"));
  let position = parseInt(action.data.position);
  // console.log(position);
  let index = findIndex(state, idTaskSwap);
  let task = tasks[index];
  task = {
    ...task,
    position: position,
  };
  tasks.splice(index, 1, task);
  localStorage.setItem("List Tasks", JSON.stringify(tasks));
  return updateObject(state, {
    tasks: tasks,
    showModalSwap: !state.showModalSwap,
  });
};

const onDeleteTask = (state, action) => {
  let tasksObject = JSON.parse(localStorage.getItem("List Tasks"));
  let tasks = [];
  for (let i = 0; i < tasksObject.length; i++) {
    tasks.push(tasksObject[i]);
  }
  let id = action.id;
  let result = -1;
  tasks.forEach((task, index) => {
    if (task.id === id) {
      result = index;
    }
  });
  let indexArray = result;
  if (indexArray !== -1) {
    const arrayTask = tasks.filter(task => task.id !== id);
    localStorage.setItem("List Tasks", JSON.stringify(arrayTask));
    return updateObject(state, {
      tasks: arrayTask,
    });
  }
};

const onCreateTask = (state, action) => {
  action.event.preventDefault();
  // let { tasks } = state;
  let tasks = JSON.parse(localStorage.getItem("List Tasks"));
  let task = action.task;
  let newTask = {
    task,
    id: Math.random() + Math.random(),
  };
  tasks.push(newTask);
  updateObject(state, {
    tasks: tasks,
  });
  localStorage.setItem("List Tasks", JSON.stringify(tasks));
};

const onChangeTaskForm = (state, action) => {
  return updateObject(state, {
    [action.event.target.name]: action.event.target.value,
  });
};

// const onChangeDetail = (state, action) => {
//   let { name, description } = state;
//   name = action.data.name;
//   description = action.data.description;

// };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    // case actionTypes.ON_CHANGE_DETAIL:
    //   return onChangeDetail(state, action);
    case actionTypes.ON_TOGGLE_MODAL:
      return onToggleModal(state, action);
    case actionTypes.ON_ADD_TASK:
      return onAddTask(state, action);
    case actionTypes.ON_CLOSE_MODAL:
      return onCloseModal(state, action);
    case actionTypes.ON_TOGGLE_MODAL_SWAP:
      return onToggleModalSwap(state, action);
    case actionTypes.ON_UPDATE_POSITION:
      return onUpdatePosition(state, action);
    case actionTypes.ON_DELETE_TASK:
      return onDeleteTask(state, action);
    case actionTypes.ON_CREATE_TASK:
      return onCreateTask(state, action);
    case actionTypes.ON_CHANGE_TASK_FORM:
      return onChangeTaskForm(state, action);
    default:
      return state;
  }
};

export default reducer;
